# Afrikaans translation for apparmor
# Copyright (c) 2020 Rosetta Contributors and Canonical Ltd 2020
# This file is distributed under the same license as the apparmor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: apparmor\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2014-09-14 19:29+0530\n"
"PO-Revision-Date: 2023-01-29 16:16+0000\n"
"Last-Translator: Mark Grassi <Unknown>\n"
"Language-Team: Afrikaans <af@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2023-01-30 06:25+0000\n"
"X-Generator: Launchpad (build 20dbca4abd50eb567cd11c349e7e914443a145a1)\n"

#: ../aa-genprof:56
msgid "Generate profile for the given program"
msgstr "Genereer profiel vir die gegewe programe"

#: ../aa-genprof:57 ../aa-logprof:25 ../aa-cleanprof:24 ../aa-mergeprof:34
#: ../aa-autodep:25 ../aa-audit:25 ../aa-complain:24 ../aa-enforce:24
#: ../aa-disable:24
msgid "path to profiles"
msgstr "pad- om profiele"

#: ../aa-genprof:58 ../aa-logprof:26
msgid "path to logfile"
msgstr "pad na loglêer"

#: ../aa-genprof:59
msgid "name of program to profile"
msgstr "naam van program na profiel"

#: ../aa-genprof:69 ../aa-logprof:37
#, python-format
msgid "The logfile %s does not exist. Please check the path"
msgstr "Die loglêer %s bestaan nie. Tjek asseblief die -pad"

#: ../aa-genprof:75 ../aa-logprof:43 ../aa-unconfined:36
msgid ""
"It seems AppArmor was not started. Please enable AppArmor and try again."
msgstr ""
"Dit lyk AppArmor was nie begin het. Aktiveer asseblief AppArmor en probeer "
"weer."

#: ../aa-genprof:80 ../aa-mergeprof:47
#, python-format
msgid "%s is not a directory."
msgstr "%s is nie 'n gids."

#: ../aa-genprof:94
#, python-format
msgid ""
"Can't find %(profiling)s in the system path list. If the name of the "
"application\n"
"is correct, please run 'which %(profiling)s' as a user with correct PATH\n"
"environment set up in order to find the fully-qualified path and\n"
"use the full path as parameter."
msgstr ""
"Kan nie vind %(profiling)s in die stelselpadlys. Indien die naam van die "
"aansoek is korrek, asseblief hardloop 'which %(profiling)s' as gebruiker met "
"korrekte PATH\n"
"omgewing opgestel in orde om die volle gekwalifiseerde pad te vind en\n"
"gebruik die volledige pad as parameter."

#: ../aa-genprof:96
#, python-format
msgid "%s does not exists, please double-check the path."
msgstr "%s nie bestaan, dubbel-tjek die pad."

#: ../aa-genprof:124
msgid ""
"\n"
"Before you begin, you may wish to check if a\n"
"profile already exists for the application you\n"
"wish to confine. See the following wiki page for\n"
"more information:"
msgstr ""
"\n"
"Voor jy begin, jy mag wens om tjek indien 'n\n"
"profiel bestaan ​​reeds vir die toepassing wat jy\n"
"wens om confine. Sien die volgende wiki bladsy vir\n"
"meer inligting:"

#: ../aa-genprof:126
msgid ""
"Please start the application to be profiled in\n"
"another window and exercise its functionality now.\n"
"\n"
"Once completed, select the \"Scan\" option below in \n"
"order to scan the system logs for AppArmor events. \n"
"\n"
"For each AppArmor event, you will be given the \n"
"opportunity to choose whether the access should be \n"
"allowed or denied."
msgstr ""
"Begin asseblief die aansoek om geprofileer te word in\n"
"'n ander venster en uitoefening funksionaliteit daarvan nou.\n"
"\n"
"Sodra dit voltooi is, kies die \"Scan\" opsie hieronder in \n"
"orde om skandering die die stelsel logs vir AppArmor gebeurtenise. \n"
"\n"
"Vir elke AppArmor gebeurtenis, jy sal gegee word om die \n"
"geleentheid om te kies whether die toegang behoort te wees \n"
"toegelaat of geweier."

#: ../aa-genprof:147
msgid "Profiling"
msgstr "Profilering"

#: ../aa-genprof:165
msgid ""
"\n"
"Reloaded AppArmor profiles in enforce mode."
msgstr ""
"\n"
"Weer gelaai AppArmor profiele in afdwing modus."

#: ../aa-genprof:166
msgid ""
"\n"
"Please consider contributing your new profile!\n"
"See the following wiki page for more information:"
msgstr ""
"\n"
"Oorweeg asseblief bydraende jou nuwe profiel!\n"
"Sien die volgende wiki bladsy vir meer inligting:"

#: ../aa-genprof:167
#, python-format
msgid "Finished generating profile for %s."
msgstr "Afgewerkte genererende profiel vir %s."

#: ../aa-logprof:24
msgid "Process log entries to generate profiles"
msgstr "Proses log inskrywings om profiele te genereer"

#: ../aa-logprof:27
msgid "mark in the log to start processing after"
msgstr "merk in die log te begin met die verwerking na"

#: ../aa-cleanprof:23
msgid "Cleanup the profiles for the given programs"
msgstr "Maak skoon die profiele vir die gegewe programme"

#: ../aa-cleanprof:25 ../aa-autodep:26 ../aa-audit:27 ../aa-complain:25
#: ../aa-enforce:25 ../aa-disable:25
msgid "name of program"
msgstr "naam van programe"

#: ../aa-cleanprof:26
msgid "Silently overwrite with a clean profile"
msgstr "Oorskryf stilweg met 'n skoon profiel"

#: ../aa-mergeprof:29
msgid "Perform a 2-way or 3-way merge on the given profiles"
msgstr "Presteer 'n 2-manier of 3-manier saamsmelt op die gegewe profiele"

#: ../aa-mergeprof:31
msgid "your profile"
msgstr "jou profiel"

#: ../aa-mergeprof:32
msgid "base profile"
msgstr "base profiel"

#: ../aa-mergeprof:33
msgid "other profile"
msgstr "ander profiel"

#: ../aa-mergeprof:67 ../apparmor/aa.py:2345
msgid ""
"The following local profiles were changed. Would you like to save them?"
msgstr "Die volgende plaaslike profiele is verander. Wil jy hulle red?"

#: ../aa-mergeprof:148 ../aa-mergeprof:430 ../apparmor/aa.py:1767
msgid "Path"
msgstr "Pad"

#: ../aa-mergeprof:149
msgid "Select the appropriate mode"
msgstr "Kies die toepaslike modus"

#: ../aa-mergeprof:166
msgid "Unknown selection"
msgstr "Onbekend seleksie"

#: ../aa-mergeprof:183 ../aa-mergeprof:209
msgid "File includes"
msgstr "Lêer sluit in"

#: ../aa-mergeprof:183 ../aa-mergeprof:209
msgid "Select the ones you wish to add"
msgstr "Seleksie die wat jy wil byvoeg"

#: ../aa-mergeprof:195 ../aa-mergeprof:222
#, python-format
msgid "Adding %s to the file."
msgstr "Toevoeging van %s na die lêer."

#: ../aa-mergeprof:199 ../apparmor/aa.py:2258
msgid "unknown"
msgstr "onbekend"

#: ../aa-mergeprof:224 ../aa-mergeprof:275 ../aa-mergeprof:516
#: ../aa-mergeprof:558 ../aa-mergeprof:675 ../apparmor/aa.py:1620
#: ../apparmor/aa.py:1859 ../apparmor/aa.py:1899 ../apparmor/aa.py:2012
#, python-format
msgid "Deleted %s previous matching profile entries."
msgstr "Geskrap %s vorige ooreenstemmende profiel inskrywings."

#: ../aa-mergeprof:244 ../aa-mergeprof:429 ../aa-mergeprof:629
#: ../aa-mergeprof:656 ../apparmor/aa.py:992 ../apparmor/aa.py:1252
#: ../apparmor/aa.py:1562 ../apparmor/aa.py:1603 ../apparmor/aa.py:1766
#: ../apparmor/aa.py:1958 ../apparmor/aa.py:1994
msgid "Profile"
msgstr "Profiel"

#: ../aa-mergeprof:245 ../apparmor/aa.py:1563 ../apparmor/aa.py:1604
msgid "Capability"
msgstr "Capvermoë"

#: ../aa-mergeprof:246 ../aa-mergeprof:480 ../apparmor/aa.py:1258
#: ../apparmor/aa.py:1564 ../apparmor/aa.py:1605 ../apparmor/aa.py:1817
msgid "Severity"
msgstr "Ernstigheidsgraad"

#: ../aa-mergeprof:273 ../aa-mergeprof:514 ../apparmor/aa.py:1618
#: ../apparmor/aa.py:1857
#, python-format
msgid "Adding %s to profile."
msgstr "Toevoeging van %s na profiel."

#: ../aa-mergeprof:282 ../apparmor/aa.py:1627
#, python-format
msgid "Adding capability %s to profile."
msgstr "Die capvermoë %s by profiel gevoeg."

#: ../aa-mergeprof:289 ../apparmor/aa.py:1634
#, python-format
msgid "Denying capability %s to profile."
msgstr "Weier die capvermoë %s om te profiel."

#: ../aa-mergeprof:439 ../aa-mergeprof:470 ../apparmor/aa.py:1776
#: ../apparmor/aa.py:1807
msgid "(owner permissions off)"
msgstr "(toestemmings vir eienaars af)"

#: ../aa-mergeprof:444 ../apparmor/aa.py:1781
msgid "(force new perms to owner)"
msgstr "(dwing nuwe permitte aan die eienaar)"

#: ../aa-mergeprof:447 ../apparmor/aa.py:1784
msgid "(force all rule perms to owner)"
msgstr "(Dwing alle heerskappy perms om eienaar)"

#: ../aa-mergeprof:459 ../apparmor/aa.py:1796
msgid "Old Mode"
msgstr "Ou Modus"

#: ../aa-mergeprof:460 ../apparmor/aa.py:1797
msgid "New Mode"
msgstr "Nuwe Modus"

#: ../aa-mergeprof:475 ../apparmor/aa.py:1812
msgid "(force perms to owner)"
msgstr "(dwing perms aan eienaar)"

#: ../aa-mergeprof:478 ../apparmor/aa.py:1815
msgid "Mode"
msgstr "Modus"

#: ../aa-mergeprof:556
#, python-format
msgid "Adding %(path)s %(mod)s to profile"
msgstr "Toevoeging %(path)s %(mod)s om profiel"

#: ../aa-mergeprof:574 ../apparmor/aa.py:1915
msgid "Enter new path: "
msgstr "Voer nuwe pad in: "

#: ../aa-mergeprof:630 ../aa-mergeprof:657 ../apparmor/aa.py:1959
#: ../apparmor/aa.py:1995
msgid "Network Family"
msgstr "Netwerk Familie"

#: ../aa-mergeprof:631 ../aa-mergeprof:658 ../apparmor/aa.py:1960
#: ../apparmor/aa.py:1996
msgid "Socket Type"
msgstr "Socket Tipe"

#: ../aa-mergeprof:673 ../apparmor/aa.py:2010
#, python-format
msgid "Adding %s to profile"
msgstr "Voeg %s by profiel"

#: ../aa-mergeprof:683 ../apparmor/aa.py:2020
#, python-format
msgid "Adding network access %(family)s %(type)s to profile."
msgstr "Voeg netwerk toegang %(family)s %(type)s na profiel."

#: ../aa-mergeprof:689 ../apparmor/aa.py:2026
#, python-format
msgid "Denying network access %(family)s %(type)s to profile"
msgstr "Ontken netwerk toegang %(family)s %(type)s na profiel"

#: ../aa-autodep:23
msgid "Generate a basic AppArmor profile by guessing requirements"
msgstr "Genereer 'n basiese AppArmor profiel deur raai van vereistes"

#: ../aa-autodep:24
msgid "overwrite existing profile"
msgstr "Oorskryf bestaande profiel"

#: ../aa-audit:24
msgid "Switch the given programs to audit mode"
msgstr "Skakelaar die gegewe programme om oudit modus"

#: ../aa-audit:26
msgid "remove audit mode"
msgstr "verwyder oudit modus"

#: ../aa-audit:28
msgid "Show full trace"
msgstr "Wys volle spore"

#: ../aa-complain:23
msgid "Switch the given program to complain mode"
msgstr "Skakelaar die gegewe programe om te kla modus"

#: ../aa-enforce:23
msgid "Switch the given program to enforce mode"
msgstr "Skakelaar die gegewe programe om afdwing modus"

#: ../aa-disable:23
msgid "Disable the profile for the given programs"
msgstr "Deaktiveer die profiel vir die gegewe programes"

#: ../aa-unconfined:28
msgid "Lists unconfined processes having tcp or udp ports"
msgstr "Lys unconfined prosesse met tcp of udp poorte"

#: ../aa-unconfined:29
msgid "scan all processes from /proc"
msgstr "skandeer alle prosesse vanaf / proc"

#: ../aa-unconfined:81
#, python-format
msgid "%(pid)s %(program)s (%(commandline)s) not confined"
msgstr "%(pid)s %(program)s (%(commandline)s) nie confined"

#: ../aa-unconfined:85
#, python-format
msgid "%(pid)s %(program)s%(pname)s not confined"
msgstr "%(pid)s %(program)s%(pname)s nie confined"

#: ../aa-unconfined:90
#, python-format
msgid "%(pid)s %(program)s (%(commandline)s) confined by '%(attribute)s'"
msgstr "%(pid)s %(program)s (%(commandline)s) confined deur '%(attribute)s'"

#: ../aa-unconfined:94
#, python-format
msgid "%(pid)s %(program)s%(pname)s confined by '%(attribute)s'"
msgstr "%(pid)s %(program)s%(pname)s confined deur '%(attribute)s'"

#: ../apparmor/aa.py:196
#, python-format
msgid "Followed too many links while resolving %s"
msgstr "Gevolg te veel links terwyl reoplossing %s"

#: ../apparmor/aa.py:252 ../apparmor/aa.py:259
#, python-format
msgid "Can't find %s"
msgstr "Kan nie vind %s"

#: ../apparmor/aa.py:264 ../apparmor/aa.py:548
#, python-format
msgid "Setting %s to complain mode."
msgstr "Instelling %s om kla modus."

#: ../apparmor/aa.py:271
#, python-format
msgid "Setting %s to enforce mode."
msgstr "Instellings %s om afdwing modus."

#: ../apparmor/aa.py:286
#, python-format
msgid "Unable to find basename for %s."
msgstr "Nie in staat om vind basenaam vir %s."

#: ../apparmor/aa.py:301
#, python-format
msgid "Could not create %(link)s symlink to %(filename)s."
msgstr "Kon skep nie %(link)s symlink om %(filename)s."

#: ../apparmor/aa.py:314
#, python-format
msgid "Unable to read first line from %s: File Not Found"
msgstr "Nie in staat om lees eerste reël vanaf %s: Lêer Nie Gevind"

#: ../apparmor/aa.py:328
#, python-format
msgid ""
"Unable to fork: %(program)s\n"
"\t%(error)s"
msgstr ""
"Nie in staat vurk: %(program)s\n"
"\t%(error)s"

#: ../apparmor/aa.py:449 ../apparmor/ui.py:303
msgid ""
"Are you sure you want to abandon this set of profile changes and exit?"
msgstr ""
"Is jy seker jy wil hierdie stel laat vaar van profiel wysigings en uitgang?"

#: ../apparmor/aa.py:451 ../apparmor/ui.py:305
msgid "Abandoning all changes."
msgstr "Laat vaar alle veranderinge."

#: ../apparmor/aa.py:464
msgid "Connecting to repository..."
msgstr "Konnekteer tans na bewaarplek..."

#: ../apparmor/aa.py:470
msgid "WARNING: Error fetching profiles from the repository"
msgstr "WAARSKUWING: Fout haal tans profiele van die repository"

#: ../apparmor/aa.py:550
#, python-format
msgid "Error activating profiles: %s"
msgstr "Fout aktiveer profiele: %s"

#: ../apparmor/aa.py:605
#, python-format
msgid "%s contains no profile"
msgstr "%s bevat geen profiel"

#: ../apparmor/aa.py:706
#, python-format
msgid ""
"WARNING: Error synchronizing profiles with the repository:\n"
"%s\n"
msgstr ""
"WAARSKUWING: Fout sinchronisasie profiele met die bewaarplek:\n"
"%s\n"

#: ../apparmor/aa.py:744
#, python-format
msgid ""
"WARNING: Error synchronizing profiles with the repository\n"
"%s"
msgstr ""
"WAARSKUWING: Fout sinchronisasie profiele met die bewaarplek\n"
"%s"

#: ../apparmor/aa.py:832 ../apparmor/aa.py:883
#, python-format
msgid ""
"WARNING: An error occurred while uploading the profile %(profile)s\n"
"%(ret)s"
msgstr ""
"WAARSKUWING: 'N fout tydens die oplaai van die profiel %(profile)s\n"
"%(ret)s"

#: ../apparmor/aa.py:833
msgid "Uploaded changes to repository."
msgstr "Opgelaaide verander na repository."

#: ../apparmor/aa.py:865
msgid "Changelog Entry: "
msgstr "Verander log inskrywing: "

#: ../apparmor/aa.py:885
msgid ""
"Repository Error\n"
"Registration or Signin was unsuccessful. User login\n"
"information is required to upload profiles to the repository.\n"
"These changes could not be sent."
msgstr ""
"Bewaarplek Fout\n"
"Registrasie of Inlog was onsuksesvol. Gebruiker aanmelding\n"
"inligting word benodig om profiele na die bewaarplek op te laai.\n"
"Hierdie veranderinge kon nie gestuur word nie."

#: ../apparmor/aa.py:995
msgid "Default Hat"
msgstr "Verstek Hoed"

#: ../apparmor/aa.py:997
msgid "Requested Hat"
msgstr "Aangevraagde Hoed"

#: ../apparmor/aa.py:1218
#, python-format
msgid "%s has transition name but not transition mode"
msgstr "%s het oorgangsnaam maar nie oorgangsmodus"

#: ../apparmor/aa.py:1232
#, python-format
msgid "Target profile exists: %s\n"
msgstr "Teiken profiel bestaan: %s\n"

#: ../apparmor/aa.py:1254
msgid "Program"
msgstr "Program"

#: ../apparmor/aa.py:1257
msgid "Execute"
msgstr "Teregstel"

#: ../apparmor/aa.py:1287
msgid "Are you specifying a transition to a local profile?"
msgstr "Is jy spesifiseer 'n oorgang na 'n plaaslike profiel?"

#: ../apparmor/aa.py:1299
msgid "Enter profile name to transition to: "
msgstr "Betree profiel naam om oorgang na: "

#: ../apparmor/aa.py:1308
msgid ""
"Should AppArmor sanitise the environment when\n"
"switching profiles?\n"
"\n"
"Sanitising environment is more secure,\n"
"but some applications depend on the presence\n"
"of LD_PRELOAD or LD_LIBRARY_PATH."
msgstr ""
"Behoort AppArmor sanitize die omgewing wanneer\n"
"oorskakel tans profiele?\n"
"\n"
"Sanitising omgewing is meer beveilig,\n"
"maar sommige toepassings hang af van die teenwoordigheid\n"
"van LD_PRELOAD or LD_LIBRARY_PATH."

#: ../apparmor/aa.py:1310
msgid ""
"Should AppArmor sanitise the environment when\n"
"switching profiles?\n"
"\n"
"Sanitising environment is more secure,\n"
"but this application appears to be using LD_PRELOAD\n"
"or LD_LIBRARY_PATH and sanitising the environment\n"
"could cause functionality problems."
msgstr ""
"Behoort AppArmor sanitize die omgewing wanneer\n"
"oorskakel tans profiele?\n"
"\n"
"Sanitising omgewing is meer beveilig,\n"
"maar hierdie toepassing verskyn om deur gebruik van LD_PRELOAD\n"
"of LD_LIBRARY_PATH en die omgewing te sanitiseer\n"
"kon funksionaliteit probleme- veroorsaak."

#: ../apparmor/aa.py:1318
#, python-format
msgid ""
"Launching processes in an unconfined state is a very\n"
"dangerous operation and can cause serious security holes.\n"
"\n"
"Are you absolutely certain you wish to remove all\n"
"AppArmor protection when executing %s ?"
msgstr ""
"Lancering tans prosesse in 'n unconfined staat is 'n heel\n"
"gevaarlike operasie en kan ernstige sekuriteitsgate veroorsaak.\n"
"\n"
"Is jy absoluut seker jy wil alle verwyder\n"
"AppArmor beskerming wanneer teregstel %s?"

#: ../apparmor/aa.py:1320
msgid ""
"Should AppArmor sanitise the environment when\n"
"running this program unconfined?\n"
"\n"
"Not sanitising the environment when unconfining\n"
"a program opens up significant security holes\n"
"and should be avoided if at all possible."
msgstr ""
"Behoort AppArmor sanitize die omgewing wanneer\n"
"hardlooping hierdie program unconfined?\n"
"\n"
"Nie sanitising die omgewing wanneer unconfining\n"
"'n program oop op beduidende sekuriteit gate\n"
"en behoort deur vermy indien enigsins moontlik."

#: ../apparmor/aa.py:1396 ../apparmor/aa.py:1414
#, python-format
msgid ""
"A profile for %s does not exist.\n"
"Do you want to create one?"
msgstr ""
"'N Profiel vir %s bestaan nie.\n"
"Wil jy om een te skep?"

#: ../apparmor/aa.py:1523
msgid "Complain-mode changes:"
msgstr "Klagte-modus veranderinge:"

#: ../apparmor/aa.py:1525
msgid "Enforce-mode changes:"
msgstr "Afdwing-modus veranderinge:"

#: ../apparmor/aa.py:1528
#, python-format
msgid "Invalid mode found: %s"
msgstr "Ongeldige modus gevind: %s"

#: ../apparmor/aa.py:1897
#, python-format
msgid "Adding %(path)s %(mode)s to profile"
msgstr "Byvoeg tans %(path)s %(mode)s om profiel"

#: ../apparmor/aa.py:1918
#, python-format
msgid ""
"The specified path does not match this log entry:\n"
"\n"
"  Log Entry: %(path)s\n"
"  Entered Path:  %(ans)s\n"
"Do you really want to use this path?"
msgstr ""
"Die gespesifiseerde pad kom nie ooreen met dié log inskrywing:\n"
"\n"
"  Log Inskrywing: %(path)s\n"
"  Betreeed Pad: %(ans)s\n"
"Wil jy regtig hierdie pad gebruik?"

#: ../apparmor/aa.py:2251
#, python-format
msgid "Reading log entries from %s."
msgstr "Lees tans log inskrywings vanaf %s."

#: ../apparmor/aa.py:2254
#, python-format
msgid "Updating AppArmor profiles in %s."
msgstr "Opdatering tans van AppArmor profiele in %s."

#: ../apparmor/aa.py:2323
msgid ""
"Select which profile changes you would like to save to the\n"
"local profile set."
msgstr ""
"Selekteer watter profielveranderings jy aan wil stoor om die\n"
"plaaslike profiel stel."

#: ../apparmor/aa.py:2324
msgid "Local profile changes"
msgstr "Plaaslike profiel veranderinge"

#: ../apparmor/aa.py:2418
msgid "Profile Changes"
msgstr "Profiel Veranderinge"

#: ../apparmor/aa.py:2428
#, python-format
msgid "Can't find existing profile %s to compare changes."
msgstr "Kon nie bestaande profiel vind %s om vergelyk veranderinge."

#: ../apparmor/aa.py:2566 ../apparmor/aa.py:2581
#, python-format
msgid "Can't read AppArmor profiles in %s"
msgstr "Kon nie AppArmor profiele in lees %s"

#: ../apparmor/aa.py:2677
#, python-format
msgid ""
"%(profile)s profile in %(file)s contains syntax errors in line: %(line)s."
msgstr ""
"%(profile)s profiel in %(file)s bevat sintaks foute in lyn: %(line)s."

#: ../apparmor/aa.py:2734
#, python-format
msgid ""
"Syntax Error: Unexpected End of Profile reached in file: %(file)s line: "
"%(line)s"
msgstr ""
"Sintaks Fout: Onverwagte Einde van Profiel bereik in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:2749
#, python-format
msgid ""
"Syntax Error: Unexpected capability entry found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Sintaks Fout: Onverwagte capvermoë inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:2770
#, python-format
msgid ""
"Syntax Error: Unexpected link entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwagte skakel inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:2798
#, python-format
msgid ""
"Syntax Error: Unexpected change profile entry found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Sintaks Fout: Onverwagte verandering profiel inskrywing gevind in lêer: "
"%(file)s lyn: %(line)s"

#: ../apparmor/aa.py:2820
#, python-format
msgid ""
"Syntax Error: Unexpected rlimit entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwagte rlimit inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:2831
#, python-format
msgid ""
"Syntax Error: Unexpected boolean definition found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Sintaks Fout: Onverwagte boolean definisie gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:2871
#, python-format
msgid ""
"Syntax Error: Unexpected bare file rule found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Sintaks Fout: Onverwagte kaal lêer reël gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:2894
#, python-format
msgid ""
"Syntax Error: Unexpected path entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwags pad inskrywing in lêer gevind: %(file)s lyn: %(line)s"

#: ../apparmor/aa.py:2922
#, python-format
msgid "Syntax Error: Invalid Regex %(path)s in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Ongeldige Regex %(path)s in lêer: %(file)s lyn: %(line)s"

#: ../apparmor/aa.py:2925
#, python-format
msgid "Invalid mode %(mode)s in file: %(file)s line: %(line)s"
msgstr "Ongeldige modus %(mode)s in lêer: %(file)s lyn: %(line)s"

#: ../apparmor/aa.py:2977
#, python-format
msgid ""
"Syntax Error: Unexpected network entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwags netwerk inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:3007
#, python-format
msgid ""
"Syntax Error: Unexpected dbus entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwags dbus inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:3030
#, python-format
msgid ""
"Syntax Error: Unexpected mount entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwags monteer inskrywing in lêer gevind: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:3052
#, python-format
msgid ""
"Syntax Error: Unexpected signal entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwags sein inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:3074
#, python-format
msgid ""
"Syntax Error: Unexpected ptrace entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwags ptrace inskrywing gevind in lêer: %(file)s lyn "
"%(line)s"

#: ../apparmor/aa.py:3096
#, python-format
msgid ""
"Syntax Error: Unexpected pivot_root entry found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Sintaks Fout: Onverwags pivot_root inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:3118
#, python-format
msgid ""
"Syntax Error: Unexpected unix entry found in file: %(file)s line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwags unix inskrywing gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:3140
#, python-format
msgid ""
"Syntax Error: Unexpected change hat declaration found in file: %(file)s "
"line: %(line)s"
msgstr ""
"Sintaks Fout: Onverwagte verander hoed deklarasie gevind in lêer: %(file)s "
"lyn: %(line)s"

#: ../apparmor/aa.py:3152
#, python-format
msgid ""
"Syntax Error: Unexpected hat definition found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Sintaks Fout: Onverwagte hoed definisie gevind in lêer: %(file)s lyn: "
"%(line)s"

#: ../apparmor/aa.py:3168
#, python-format
msgid "Error: Multiple definitions for hat %(hat)s in profile %(profile)s."
msgstr ""
"Fout: Veelvuldige definisies vir hoed %(hat)s in profiel %(profile)s."

#: ../apparmor/aa.py:3185
#, python-format
msgid "Warning: invalid \"REPOSITORY:\" line in %s, ignoring."
msgstr "Waarskuwing: ongeldig \"REPOSITORY:\" lyn in %s, ignoreer tans."

#: ../apparmor/aa.py:3198
#, python-format
msgid "Syntax Error: Unknown line found in file: %(file)s line: %(line)s"
msgstr "Sintaks Fout: Onbekende lyn gevind in lêer: %(file)s lyn: %(line)s"

#: ../apparmor/aa.py:3211
#, python-format
msgid ""
"Syntax Error: Missing '}' or ','. Reached end of file %(file)s while inside "
"profile %(profile)s"
msgstr ""
"Sintaks Fout: Ontbreek '}' of ','. Bereikd einde van lêer %(file)s terwyl "
"binnekant profiel %(profile)s"

#: ../apparmor/aa.py:3277
#, python-format
msgid "Redefining existing variable %(variable)s: %(value)s in %(file)s"
msgstr ""
"Herdefiniëring bestaande veranderlike %(variable)s: %(value)s in %(file)s"

#: ../apparmor/aa.py:3282
#, python-format
msgid ""
"Values added to a non-existing variable %(variable)s: %(value)s in %(file)s"
msgstr ""
"Waardes bygevoeg tot a nie-bestaande veranderlike %(variable)s: %(value)s in "
"%(file)s"

#: ../apparmor/aa.py:3284
#, python-format
msgid ""
"Unknown variable operation %(operation)s for variable %(variable)s in "
"%(file)s"
msgstr ""
"Onbekende veranderlike bewerking %(operation)s vir veranderlike %(variable)s "
"in %(file)s"

#: ../apparmor/aa.py:3343
#, python-format
msgid "Invalid allow string: %(allow)s"
msgstr "Ongeldig toelaat string: %(allow)s"

#: ../apparmor/aa.py:3778
msgid "Can't find existing profile to modify"
msgstr "Kon nie bestaande profiel vind modify"

#: ../apparmor/aa.py:4347
#, python-format
msgid "Writing updated profile for %s."
msgstr "Skryf opgedateer profiel vir %s."

#: ../apparmor/aa.py:4481
#, python-format
msgid "File Not Found: %s"
msgstr "Lêer nie gevind: %s"

#: ../apparmor/aa.py:4591
#, python-format
msgid ""
"%s is currently marked as a program that should not have its own\n"
"profile.  Usually, programs are marked this way if creating a profile for \n"
"them is likely to break the rest of the system.  If you know what you're\n"
"doing and are certain you want to create a profile for this program, edit\n"
"the corresponding entry in the [qualifiers] section in "
"/etc/apparmor/logprof.conf."
msgstr ""
"%s is tans gemerk as 'n program wat nie sy eie moet hê\n"
"profiel. Gewoonlik, programme word op hierdie manier gemerk indien 'n "
"profiel vir \n"
"hulle sal waarskynlik die res van die stelsel breek. Indien jy weet wat jy\n"
"doen en sekere jy wil 'n profiel te skep vir hierdie program, wysig\n"
"die ooreenstemmende inskrywing in die [qualifiers] gedeelte in "
"/etc/apparmor/logprof.conf."

#: ../apparmor/logparser.py:127 ../apparmor/logparser.py:132
#, python-format
msgid "Log contains unknown mode %s"
msgstr "Log bevat onbekende modus %s"

#: ../apparmor/tools.py:84 ../apparmor/tools.py:126
#, python-format
msgid ""
"Can't find %(program)s in the system path list. If the name of the "
"application\n"
"is correct, please run 'which %(program)s' as a user with correct PATH\n"
"environment set up in order to find the fully-qualified path and\n"
"use the full path as parameter."
msgstr ""
"Kan nie vind %(program)s in die stelselpadlys. As die naam van die aansoek\n"
"is korrek, asseblief hardloop 'which %(program)s' as gebruiker met korrekte "
"PATH\n"
"omgewing opgestel ten einde om vind die volledig gekwalifiseerde pad en\n"
"gebruik die volledige pad as parameter."

#: ../apparmor/tools.py:86 ../apparmor/tools.py:102 ../apparmor/tools.py:128
#, python-format
msgid "%s does not exist, please double-check the path."
msgstr "%s bestaan nie, asseblief dubbel seker die pad."

#: ../apparmor/tools.py:100
msgid ""
"The given program cannot be found, please try with the fully qualified path "
"name of the program: "
msgstr ""
"Die gegewe programe kan nie gevind, probeer asseblief met die volledig "
"gekwalifiseerde pad naam van die program: "

#: ../apparmor/tools.py:113 ../apparmor/tools.py:137 ../apparmor/tools.py:157
#: ../apparmor/tools.py:175 ../apparmor/tools.py:193
#, python-format
msgid "Profile for %s not found, skipping"
msgstr "Profiel vir %s nie gevind, oorslaan"

#: ../apparmor/tools.py:140
#, python-format
msgid "Disabling %s."
msgstr "Deaktiveer tans %s."

#: ../apparmor/tools.py:198
#, python-format
msgid "Setting %s to audit mode."
msgstr "Instelling %s om oudit modus."

#: ../apparmor/tools.py:200
#, python-format
msgid "Removing audit mode from %s."
msgstr "Verwyder tans oudit modus vanaf %s."

#: ../apparmor/tools.py:212
#, python-format
msgid ""
"Please pass an application to generate a profile for, not a profile itself - "
"skipping %s."
msgstr ""
"Asseblief slaag 'n program om 'n profiel vir, nie 'n profiel self - huppel "
"tans %s."

#: ../apparmor/tools.py:220
#, python-format
msgid "Profile for %s already exists - skipping."
msgstr "Profiel vir %s bestaan alreeds - huppel tans."

#: ../apparmor/tools.py:232
#, python-format
msgid ""
"\n"
"Deleted %s rules."
msgstr ""
"\n"
"Geskrap %s reëls."

#: ../apparmor/tools.py:240
#, python-format
msgid ""
"The local profile for %(program)s in file %(file)s was changed. Would you "
"like to save it?"
msgstr ""
"Die plaaslike profiel vir %(program)s in lêer %(file)s was verander. Sou jy "
"hou om dit stoor?"

#: ../apparmor/tools.py:260
#, python-format
msgid "The profile for %s does not exists. Nothing to clean."
msgstr "Die profiel vir %s doen nie bestaan. Niks om skoon maak."

#: ../apparmor/ui.py:61
msgid "Invalid hotkey for"
msgstr "Ongeldig hotkey vir"

#: ../apparmor/ui.py:77 ../apparmor/ui.py:121 ../apparmor/ui.py:275
msgid "(Y)es"
msgstr "(J)a"

#: ../apparmor/ui.py:78 ../apparmor/ui.py:122 ../apparmor/ui.py:276
msgid "(N)o"
msgstr "(G)een"

#: ../apparmor/ui.py:123
msgid "(C)ancel"
msgstr "(K)anselleer"

#: ../apparmor/ui.py:223
msgid "(A)llow"
msgstr "(T)oelaat"

#: ../apparmor/ui.py:224
msgid "(M)ore"
msgstr "(M)eer"

#: ../apparmor/ui.py:225
msgid "Audi(t)"
msgstr "O(u)dit"

#: ../apparmor/ui.py:226
msgid "Audi(t) off"
msgstr "O(u)dit af"

#: ../apparmor/ui.py:227
msgid "Audit (A)ll"
msgstr "Oudit (A)lle"

#: ../apparmor/ui.py:229
msgid "(O)wner permissions on"
msgstr "Eienaar(s) toestemmings op"

#: ../apparmor/ui.py:230
msgid "(O)wner permissions off"
msgstr "Eienaar(s) toestemmings af"

#: ../apparmor/ui.py:231
msgid "(D)eny"
msgstr "(O)ntken"

#: ../apparmor/ui.py:232
msgid "Abo(r)t"
msgstr "Abo(r)teer"

#: ../apparmor/ui.py:233
msgid "(F)inish"
msgstr "A(f)werking"

#: ../apparmor/ui.py:234
msgid "(I)nherit"
msgstr "(B)eërwe"

#: ../apparmor/ui.py:235
msgid "(P)rofile"
msgstr "(P)rofiel"

#: ../apparmor/ui.py:236
msgid "(P)rofile Clean Exec"
msgstr "(P)rofiel Skoon Exec"

#: ../apparmor/ui.py:237
msgid "(C)hild"
msgstr "(K)ind"

#: ../apparmor/ui.py:238
msgid "(C)hild Clean Exec"
msgstr "(K)ind Skoon Exec"

#: ../apparmor/ui.py:239
msgid "(N)amed"
msgstr "(N)amed"

#: ../apparmor/ui.py:240
msgid "(N)amed Clean Exec"
msgstr "(N)amed Skoon Exec"

#: ../apparmor/ui.py:241
msgid "(U)nconfined"
msgstr "(U)nconfined"

#: ../apparmor/ui.py:242
msgid "(U)nconfined Clean Exec"
msgstr "(U)nconfined Skoon Exec"

#: ../apparmor/ui.py:243
msgid "(P)rofile Inherit"
msgstr "(P)rofiel Inherit"

#: ../apparmor/ui.py:244
msgid "(P)rofile Inherit Clean Exec"
msgstr "(P)rofiel Inherit Skoon Exec"

#: ../apparmor/ui.py:245
msgid "(C)hild Inherit"
msgstr "(K)ind Inherit"

#: ../apparmor/ui.py:246
msgid "(C)hild Inherit Clean Exec"
msgstr "(K)ind Inherit Skoon Exec"

#: ../apparmor/ui.py:247
msgid "(N)amed Inherit"
msgstr "(V)ernoem Inherit"

#: ../apparmor/ui.py:248
msgid "(N)amed Inherit Clean Exec"
msgstr "(V)ernoem Inherit Skoon Exec"

#: ../apparmor/ui.py:249
msgid "(X) ix On"
msgstr "(X) ix Op"

#: ../apparmor/ui.py:250
msgid "(X) ix Off"
msgstr "(X) ix Af"

#: ../apparmor/ui.py:251 ../apparmor/ui.py:265
msgid "(S)ave Changes"
msgstr "(S)toor Veranderinge"

#: ../apparmor/ui.py:252
msgid "(C)ontinue Profiling"
msgstr "(V)oortgaan Profilering"

#: ../apparmor/ui.py:253
msgid "(N)ew"
msgstr "(N)uwe"

#: ../apparmor/ui.py:254
msgid "(G)lob"
msgstr "(G)lob"

#: ../apparmor/ui.py:255
msgid "Glob with (E)xtension"
msgstr "Glob with (E)kstensie"

#: ../apparmor/ui.py:256
msgid "(A)dd Requested Hat"
msgstr "(V)oeg Gevraagde Hoed"

#: ../apparmor/ui.py:257
msgid "(U)se Default Hat"
msgstr "(G)ebruik Verstek Hoed"

#: ../apparmor/ui.py:258
msgid "(S)can system log for AppArmor events"
msgstr "(S)kandering stelsel log vir AppArmor gebeure"

#: ../apparmor/ui.py:259
msgid "(H)elp"
msgstr "(H)ulp"

#: ../apparmor/ui.py:260
msgid "(V)iew Profile"
msgstr "(B)esigtig Profiel"

#: ../apparmor/ui.py:261
msgid "(U)se Profile"
msgstr "(G)ebruik Profiel"

#: ../apparmor/ui.py:262
msgid "(C)reate New Profile"
msgstr "(S)kep Nuwe Profiel"

#: ../apparmor/ui.py:263
msgid "(U)pdate Profile"
msgstr "(O)pdateer Profiel"

#: ../apparmor/ui.py:264
msgid "(I)gnore Update"
msgstr "(I)gnoreer Opdatering"

#: ../apparmor/ui.py:266
msgid "Save Selec(t)ed Profile"
msgstr "Stoor Gekos(e) Profiel"

#: ../apparmor/ui.py:267
msgid "(U)pload Changes"
msgstr "(O)plaai Wysigings"

#: ../apparmor/ui.py:268
msgid "(V)iew Changes"
msgstr "(U)itsig Veranderinge"

#: ../apparmor/ui.py:269
msgid "View Changes b/w (C)lean profiles"
msgstr "Uitsig Veranderinge b/w Skoon (p)rofiele"

#: ../apparmor/ui.py:270
msgid "(V)iew"
msgstr "(U)itsig"

#: ../apparmor/ui.py:271
msgid "(E)nable Repository"
msgstr "(A)ktiveer Bewaarplek"

#: ../apparmor/ui.py:272
msgid "(D)isable Repository"
msgstr "(D)eaktiveer Bewaarplek"

#: ../apparmor/ui.py:273
msgid "(N)ever Ask Again"
msgstr "(V)ra Nooit Weer"

#: ../apparmor/ui.py:274
msgid "Ask Me (L)ater"
msgstr "Vra My (L)ater"

#: ../apparmor/ui.py:277
msgid "Allow All (N)etwork"
msgstr "Toelaat Alle (N)etwerk"

#: ../apparmor/ui.py:278
msgid "Allow Network Fa(m)ily"
msgstr "Toelaat Netwerk Fa(m)ilie"

#: ../apparmor/ui.py:279
msgid "(O)verwrite Profile"
msgstr "(O)orskryf Profiel"

#: ../apparmor/ui.py:280
msgid "(K)eep Profile"
msgstr "(B)ewaar Profiel"

#: ../apparmor/ui.py:281
msgid "(C)ontinue"
msgstr "(V)oortgaan"

#: ../apparmor/ui.py:282
msgid "(I)gnore"
msgstr "(I)gnoreer"

#: ../apparmor/ui.py:344
#, python-format
msgid "PromptUser: Unknown command %s"
msgstr "VinnigeGebruiker: Onbekende bevel %s"

#: ../apparmor/ui.py:351
#, python-format
msgid "PromptUser: Duplicate hotkey for %(command)s: %(menutext)s "
msgstr "Prompt Gebruiker: Duplikaat sneltoets vir %(command)s: %(menutext)s "

#: ../apparmor/ui.py:363
msgid "PromptUser: Invalid hotkey in default item"
msgstr "PromptGebruiker: Ongeldige sneltoets in verstek item"

#: ../apparmor/ui.py:368
#, python-format
msgid "PromptUser: Invalid default %s"
msgstr "PromptGebruiker: Ongeldige verstek %s"
